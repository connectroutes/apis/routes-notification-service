package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"routes-notification-service/proto"
)
import _ "github.com/joho/godotenv/autoload"

func main() {
	fmt.Println("Starting Server")
	lis, err := net.Listen("tcp", "0.0.0.0:50053")

	if err != nil {
		log.Fatalf("Could not listen on port: %v", err)
	}

	server := grpc.NewServer()

	proto.RegisterNotificationServiceServer(server, &service{})

	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}

}
