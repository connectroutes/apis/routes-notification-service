module routes-notification-service

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/mailgun/mailgun-go/v3 v3.6.4
	github.com/sfreiberg/gotwilio v0.0.0-20200219192335-4472db517000
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	golang.org/x/tools v0.0.0-20190524140312-2c0ae7006135 // indirect
	google.golang.org/grpc v1.26.0
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
)
