package main

import (
	"context"
	"fmt"
	"github.com/mailgun/mailgun-go/v3"
	"github.com/sfreiberg/gotwilio"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"os"
	"routes-notification-service/proto"
)

var mg = mailgun.NewMailgun(os.Getenv("MAILGUN_DOMAIN_NAME"), os.Getenv("MAILGUN_API_KEY"))

var twilio = gotwilio.NewTwilioClient(os.Getenv("TWILIO_SID"), os.Getenv("TWILIO_TOKEN"))

type service struct {
}

func (s *service) SendSms(ctx context.Context, req *proto.SendSmsRequest) (*proto.SendSmsResponse, error) {
	//TODO send sms
	fmt.Println("to send sms")

	response, exception, err := twilio.SendSMS(os.Getenv("TWILIO_NUMBER"), req.PhoneNumber, req.Message, "", "")

	if err != nil || exception != nil {
		fmt.Println(err)
		fmt.Println(exception)
		return nil, status.Errorf(codes.Internal,
			"Could not send SMS")
	}

	fmt.Println(response) //TODO remove
	return &proto.SendSmsResponse{
		Status: 1,
	}, nil
}

func (s *service) SendEmail(ctx context.Context, req *proto.SendEmailRequest) (*proto.SendEmailResponse, error) {
	fmt.Println("to send email")
	message := mg.NewMessage(req.Sender, req.Subject, req.Message, req.Recipient)

	_, _, err := mg.Send(ctx, message)

	if err == nil {
		return &proto.SendEmailResponse{
			Ok: true,
		}, nil
	} else {
		fmt.Println(err)
		return nil, status.Errorf(codes.Internal,
			"Could not send email")
	}

}
